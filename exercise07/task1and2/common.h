#define _GNU_SOURCE
#define _POSIX_SOURCE

#ifndef _STDLIB_H
#include <stdlib.h>
#endif

#ifndef _STDIO_H
#include <stdio.h>
#endif

#include "myqueue.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <ctype.h>
#include <pthread.h>

#define THREAD_NO 5
#define ENTRIES_NO 10000

void error_handler(char* msg){
    fcloseall();
    perror(msg);
    exit(EXIT_FAILURE);
}

void error_handler_thread(char* msg){
    perror(msg);
    pthread_exit(NULL);
}

struct function_parameter{
    pthread_mutex_t* mutex;
    pthread_cond_t* condition;
};

