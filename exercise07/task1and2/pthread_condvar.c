#include "common.h"

void* consumer(void* arg){
    //sum for sum, value for read value
    unsigned sum = 0;
    unsigned value;

    struct function_parameter *param = (struct function_parameter*) arg;

    pthread_mutex_t* mutex = (pthread_mutex_t*) param->mutex;
    pthread_cond_t* cond = (pthread_cond_t*) param->condition;


    while(1){
        //lock mutex
        pthread_mutex_lock(mutex);

        //when Q is not empty
        //use empty over front()!= 0 because front could lead to an error

        //when queue is empty -> wait for condition and free mutex variable, so other threads can access it
        //when pthread_cond_wait returns, mutex is locked again
        while(empty()){
            pthread_cond_wait(cond,mutex);
        }

        if(!empty()){

            //read and remove first value
            value = front();
            pop();

            //if value is 1 add it, if 0 break and free mutex
            if(value == 1){
                sum += value;
            } else {
                pthread_mutex_unlock(mutex);
                break;
            }

        }

        //when finished and not breaked, unlock mutex
        pthread_mutex_unlock(mutex);
    }

    //print sum
    fprintf(stdout,"%ld: %u\n",pthread_self(),sum);
    pthread_exit(NULL);

}

int main(void){
    pthread_t thread_vector[THREAD_NO];

    //create queue
    create();

    //create MUTEX
    pthread_mutex_t mut = PTHREAD_MUTEX_INITIALIZER;
    pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

    struct function_parameter param;
    param.condition = &cond;
    param.mutex = &mut;

    //create THREAD_NO threads
    for(int i = 0; i<THREAD_NO; i++){

        if( pthread_create(&thread_vector[i],NULL,consumer,(void*) &param) != 0 )
            error_handler("thread_create");

    }

    //writing 1s to Q
    for(int i = 0; i<ENTRIES_NO; i++){
        //lock mutex
        pthread_mutex_lock(&mut);

        //write
        push(1);

        //signal single thread which is waiting for queue elements
        pthread_cond_signal(&cond);

        //unlock mutex
        pthread_mutex_unlock(&mut);
    }

    //writing 0s to Q
    for(int i = 0; i<THREAD_NO; i++){
        //lock mutex
        pthread_mutex_lock(&mut);

        pthread_cond_signal(&cond);
        //write
        push(0);

        //unlock mutex
        pthread_mutex_unlock(&mut);
    }

    //wait for threads
    for(int i = 0; i<THREAD_NO; i++){
        pthread_join(thread_vector[i],NULL);
    }

    //destroy mutex
    if( pthread_mutex_destroy(&mut) != 0 )
        error_handler("pthread_mutex_destroy");

    //destroy condition variable
    if( pthread_cond_destroy(&cond) != 0 )
        error_handler("pthread_cond_destroy");

    return EXIT_SUCCESS;

}
