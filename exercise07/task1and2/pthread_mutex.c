#include "common.h"

void* consumer(void* arg){
    //sum for sum, value for read value
    unsigned sum = 0;
    unsigned value;

    pthread_mutex_t* mutex = (pthread_mutex_t*) arg;


    while(1){
        //lock mutex
        pthread_mutex_lock(mutex);

        //when Q is not empty
        //use empty over front()!= 0 because front could lead to an error
        if(!empty()){


            //read and remove first value
            value = front();
            pop();

            //if value is 1 add it, if 0 break and free mutex
            if(value == 1){
                sum += value;
            } else {
                pthread_mutex_unlock(mutex);
                break;
            }

        }

        //when finished and not breaked, unlock mutex
        pthread_mutex_unlock(mutex);
    }

    //print sum
    //fprintf(stdout,"%ld: %u\n",pthread_self(),sum);
    pthread_exit(NULL);

}

int main(void){
    pthread_t thread_vector[THREAD_NO];

    //create queue
    create();

    //create MUTEX
    pthread_mutex_t mut = PTHREAD_MUTEX_INITIALIZER;

    //create THREAD_NO threads
    for(int i = 0; i<THREAD_NO; i++){

        if( pthread_create(&thread_vector[i],NULL,consumer,(void*) &mut) != 0 )
            error_handler("thread_create");

    }

    //writing 1s to Q
    for(int i = 0; i<ENTRIES_NO; i++){
        //lock mutex
        pthread_mutex_lock(&mut);

        //write
        push(1);

        //unlock mutex
        pthread_mutex_unlock(&mut);
    }

    //writing 0s to Q
    for(int i = 0; i<THREAD_NO; i++){
        //lock mutex
        pthread_mutex_lock(&mut);

        //write
        push(0);

        //unlock mutex
        pthread_mutex_unlock(&mut);
    }

    //wait for threads
    for(int i = 0; i<THREAD_NO; i++){
        pthread_join(thread_vector[i],NULL);
    }

    //destroy mutex
    pthread_mutex_destroy(&mut);

    return EXIT_SUCCESS;

}
