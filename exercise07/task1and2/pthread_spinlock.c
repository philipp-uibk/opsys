#include "common.h"

void* consumer(void* arg){
    //sum for sum, value for read value
    unsigned sum = 0;
    unsigned value;

    pthread_spinlock_t *spinlock = (pthread_spinlock_t *) arg;

    while(1){
        //lock spinlock
        pthread_spin_lock(spinlock);

        //when Q is not empty
        //use empty over front()!= 0 because front could lead to an error
        if(!empty()){

            //read and remove first value
            value = front();
            pop();

            //if value is 1 add it, if 0 break and free spinlock
            if(value == 1){
                sum += value;
            } else {
                pthread_spin_unlock(spinlock);
                break;
            }

        }

        //when finished and not breaked, unlock spinlock
        pthread_spin_unlock(spinlock);
    }

    //print sum
    //fprintf(stdout,"%ld: %u\n",pthread_self(),sum);
    pthread_exit(NULL);

}

int main(void){
    pthread_t thread_vector[THREAD_NO];

    //create queue
    create();

    //create SPINLOCK variable
    pthread_spinlock_t spinlock_var;

    if( pthread_spin_init(&spinlock_var,PTHREAD_PROCESS_SHARED) != 0 )
        error_handler("pthread_spin_init");

    //create THREAD_NO threads
    for(int i = 0; i<THREAD_NO; i++){

        if( pthread_create(&thread_vector[i],NULL,consumer,(void*) &spinlock_var) != 0 )
            error_handler("thread_create");

    }

    //writing 1s to Q
    for(int i = 0; i<ENTRIES_NO; i++){
        //lock spinlock
        pthread_spin_lock(&spinlock_var);

        //write
        push(1);

        //unlock spinlock
        pthread_spin_unlock(&spinlock_var);
    }

    //writing 0s to Q
    for(int i = 0; i<THREAD_NO; i++){
        //lock spinlock
        pthread_spin_lock(&spinlock_var);

        //write
        push(0);

        //unlock spinlock
        pthread_spin_unlock(&spinlock_var);
    }

    //wait for threads
    for(int i = 0; i<THREAD_NO; i++){
        pthread_join(thread_vector[i],NULL);
    }

    //destroy spinlock variable
    if( pthread_spin_destroy(&spinlock_var) != 0)
        error_handler("pthread_spin_destroy");

    return EXIT_SUCCESS;

}
