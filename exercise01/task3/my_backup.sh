#!/bin/bash
if [ "$#" -ne 1 ] #if not only one parameter is given to the script, an error will be displayed
    then
    echo "bash script.sh /directory/"
    exit 1
fi

BACPATH=$HOME$1 #set path to home/path/
CHECK=1 # a checkvariable

if [ -d "$BACPATH" ] #if our path is already a directory then
    then
    for CURNAME in * #for every file in origpath
        do 
        for BACNAME in $BACPATH* #go through all files in backuppath
            do 
            CURNAME=`basename $CURNAME` #get the names of the files
            BACNAME=`basename $BACNAME` 
            if [ "$CURNAME" = "$BACNAME" ] #if two files have the same name
                then
                CHECK=0                         # set check to false
                TIMECUR=`stat --format %Y $CURNAME`         #get timestamps of each of the files
                TIMEBAC=`stat --format %Y $BACPATH$BACNAME`
                
                if [ $TIMECUR -gt $TIMEBAC ] #if the timestamp of the current path is higher than the backup path 
                    then
                    echo "done"
                    cp "$CURNAME" "$BACPATH"  #do the backup
                fi   #nothing else
                
            fi
             
        done
        if [ $CHECK -eq 1 ]   # if we have looped over the whole backup directory and didnt find a file which has the same filename
        	then
        	cp "$CURNAME" "$BACPATH" #copy the file
        fi
        
    done
    else   #case: backup-directory doesnt exist
    mkdir -p $BACPATH  #mkdir with parents to the path given
    cp * $BACPATH #copy everything to our backuppath
fi

