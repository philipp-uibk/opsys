#!/bin/bash
#script is called with bash task2.sh <input> <output>
#it takes the content of input and overwrites output with the content of input
#if output isnt writeable an error message will display
#if there are less or more than 2 parameters an error message will display

function arg_err {
    if [ $ARGC -ne 2 ] #if argc != 2
    then
        echo "bash script.sh <infile> <outfile>" #write to stdout
        exit 1 
    fi
}


ARGC=$# #saving value of $#
arg_err #calling function arg_err
INFILE=$1 
OUTFILE=$2

if [ -e "$INFILE" ] #if infile exists
then
    if [ -w "$OUTFILE" ] #if outfile is writeable
    then
        cat "$INFILE" >> "$OUTFILE" #write content of infile to outfile, >> redirects stdout and overwrites existing file
    else
        echo "Could not write to outfile, check permissions" > "error.log" #if not write to error.log, append if present
    fi
fi