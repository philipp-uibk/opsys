Script 1

#!/bin/bash
for FN in "$@" #for each String in parameter list
do
    chmod 0750 "$FN" #change rights to RWXR-X---
done

Takes a file list as parameter or * for all files in the current directory and changes the permissions of of each file to RWX R-X ---
If the parameter list is omitted nothing will happen.

Script 2

#!/bin/bash
function usage {
    echo "$0: <TODO: fill me in>" #when function is called, the message will be displayed
    exit 1 #and the programm will exit with status 1
}

#3 arguments are necessary, if one is missing, call usage
#shift = shift 1, shifts parameter list to the left and deletes the leftest argument
ARG1=$1; shift || usage #save first argument FILE in which grep should search, if not possible call usage
ARG2=$1; shift || usage #save second argument FILE or stream to write the result, if not possible call usage
ARG3=$1; shift || usage #save third argument PATTERN, if not possible call usage

grep -n "$ARG3" "$ARG1" > "$ARG2" #grep [OPTIONS] PATTERN [FILE] > [FILE] (the greater sign redirects stdout to $ARG2)

script2.sh <input> <output> <pattern> does the following:
It searches the given pattern in input and writes (appends) the result to output
If less then 3 parameters are comitted, a function usage is called, which will print an error message on screen
If more then 3 parameters are comitted, only the first 3 are used