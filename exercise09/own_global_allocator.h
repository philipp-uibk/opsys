#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>
#include <limits.h>

#define MEMSIZE 1024*1024*1024

typedef struct memory {
    int valid;
    unsigned size;
    struct memory* next;
} memseg_t;

void *memory;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int init_own_memory(){
    if ((memory = malloc(MEMSIZE)) == NULL){
        perror("real malloc");
        return EXIT_FAILURE;
    }

    //Initiate memory
    memseg_t *part_memory = (memseg_t *) memory;
    part_memory->valid = 0;
    part_memory->next = NULL;
    part_memory->size = MEMSIZE - sizeof(memseg_t);

    return EXIT_SUCCESS;
}

void free_own_memory(){
    free(memory);
    pthread_mutex_destroy(&mutex);
}

void *own_malloc(unsigned call_size) {
    memseg_t *currentmemory = (memseg_t *) memory;
    void *resultmemory = NULL;
    unsigned min_size = INT_MAX;

    //find smallest block for size
    pthread_mutex_lock(&mutex);
    do {
        if (currentmemory->valid == 0 && currentmemory->size >= call_size && min_size > currentmemory->size) {
            min_size = currentmemory->size;
            resultmemory = (void *) currentmemory;
        }
    } while ((currentmemory = currentmemory->next) != NULL);

    //when there is no block, which fits
    if (resultmemory == NULL) {
        return NULL;
    }

    memseg_t *first_segment = (memseg_t *) resultmemory;
    //space needs at least reserved and 1 info for additional space
    if (call_size +  sizeof(memseg_t) < first_segment->size) {
        //make a new segment, which should begin at start position + one time sizeof meminfo + callsize
        memseg_t *second_segment = resultmemory + sizeof(memseg_t) + call_size;

        //set next segment of second segment to first next, overwrite first next with second (like linked list)
        second_segment->next = first_segment->next;
        first_segment->next = second_segment;

        //second size = first_segment.size - reserved size - size of second meminfo, because first meminfo is already subtracted
        second_segment->size = first_segment->size - call_size - sizeof(memseg_t);
        second_segment->valid = 0;

        //set memsize of first segment to call_size
        first_segment->size = call_size;
    }
    first_segment->valid = 1;

    pthread_mutex_unlock(&mutex);
    return resultmemory + sizeof(memseg_t);
}

void own_free(void *ptr) {
    pthread_mutex_lock(&mutex);
    memseg_t *memory_to_free = ptr - sizeof(memseg_t);
    memory_to_free->valid = 0;

    //check if there are blocks to merge
    memseg_t *currentmemory = (memseg_t *) memory;
    memseg_t *next;

    while (currentmemory != NULL && (next = currentmemory->next) != NULL) {
        if (currentmemory->valid == 0 && next->valid == 0) {
            currentmemory->next = next->next;
            currentmemory->size += (sizeof(memseg_t) + next->size);

            next->size = 0;
            next->next = NULL;
        }

        currentmemory = currentmemory->next;
    }

    pthread_mutex_unlock(&mutex);
}
