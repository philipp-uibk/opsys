#include "common.h"

void *allocator(void *arg) {
    threadarg_t *threadarg = (threadarg_t *) arg;
    void *adress_vector[threadarg->ALLOC_NO];

    //allocate memory alloc times often
    for (int i = 0; i < threadarg->ALLOC_NO; i++) {
        //alloc allocate size
        if ((adress_vector[i] = malloc(threadarg->ALLOC_SZ)) == NULL)
            error_handler_thread("malloc", i, adress_vector);
    }

    //free everything
    for (int i = 0; i < threadarg->ALLOC_NO; i++) {
        free(adress_vector[i]);
    }

    pthread_exit(NULL);
}


int main(int argc, char *argv[]) {
    if (argc != 4)
        error_handler("argc");

    int THREAD_NO = (int) strtol(argv[1], NULL, 10);

    threadarg_t threadarg;
    threadarg.ALLOC_NO = (int) strtol(argv[2], NULL, 10);
    threadarg.ALLOC_SZ = (int) strtol(argv[3], NULL, 10);

    pthread_t thread_vector[THREAD_NO];

    //create THREAD_NO threads
    for (int i = 0; i < THREAD_NO; i++) {
        if (pthread_create(&thread_vector[i], NULL, allocator, (void *) &threadarg) != 0)
            error_handler("thread_create");
    }

    //wait for threads
    for (int i = 0; i < THREAD_NO; i++) {
        if (pthread_join(thread_vector[i], NULL) != 0)
            perror("pthread_join");
    }

    return EXIT_SUCCESS;
}

