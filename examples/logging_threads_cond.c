#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdbool.h>

#define NAME_SIZE 32
#define THREAD_NO 3
#define BUF_SIZE 128
#define ITERATIONS 5

typedef struct thread_argument {
    pthread_mutex_t *mutex;
    pthread_cond_t *not_empty;
    pthread_cond_t *empty;
    char name[NAME_SIZE];
} arg_t;

typedef struct global_memory {
    int ticket;
    char content[BUF_SIZE];
} memory_t;

memory_t thread_shared_memory;

void error_handler(char *msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}


void *producer_thread(void *arg) {
    arg_t *argument = (arg_t *) arg;
    for (int i = 0; i < ITERATIONS; i++) {
        //lock mutex
        pthread_mutex_lock(argument->mutex);

        //if ticket is 0, logger has to read data first
        //we wait until logger has emptied the field
        while (thread_shared_memory.ticket == 0) {
            pthread_cond_wait(argument->empty, argument->mutex);
        }

        //ticket now holds a valid ticket, which this thread got
        //write to memory
        sprintf(thread_shared_memory.content, "%d: %s", thread_shared_memory.ticket, argument->name);


        //invalidate ticket
        thread_shared_memory.ticket = 0;

        //now logger must work, information is not empty
        pthread_cond_signal(argument->not_empty);

        //unlock mutex
        pthread_mutex_unlock(argument->mutex);

    }
    pthread_exit(NULL);
}

void *logger_thread(void *arg) {
    arg_t *argument = (arg_t *) arg;

    for (int i = 1; i <= THREAD_NO * ITERATIONS + 1; i++) {
        //lock mutex
        pthread_mutex_lock(argument->mutex);

        //means that ticket is valid, so a producer has to work first
        while (thread_shared_memory.ticket != 0) {
            pthread_cond_wait(argument->not_empty, argument->mutex);
        }

        //now ticket is 0, so we have to log the data
        fprintf(stdout, "%s\n", thread_shared_memory.content);

        //assume here, that data got saved
        thread_shared_memory.ticket = i;

        //now one producer has to work
        pthread_cond_signal(argument->empty);

        pthread_mutex_unlock(argument->mutex);
    }

    pthread_exit(NULL);
}


int main(void) {
    //producers
    pthread_t thread_id_vector[THREAD_NO];

    //logger
    pthread_t logger_id;
    //just for initializing
    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_cond_t cond1 = PTHREAD_COND_INITIALIZER;
    pthread_cond_t cond2 = PTHREAD_COND_INITIALIZER;

    arg_t web_arg = {&mutex, &cond1, &cond2};
    arg_t data_arg = {&mutex, &cond1, &cond2};
    arg_t weather_arg = {&mutex, &cond1, &cond2};
    arg_t logger_arg = {&mutex, &cond1, &cond2};

    thread_shared_memory.ticket = 0;

    sprintf(web_arg.name, "web_server");
    sprintf(data_arg.name, "database");
    sprintf(weather_arg.name, "weather_station");
    sprintf(logger_arg.name, "logger");


    if (pthread_create(&thread_id_vector[0], NULL, producer_thread, (void *) &web_arg) != 0)
        error_handler("pthread_create");

    if (pthread_create(&thread_id_vector[1], NULL, producer_thread, (void *) &data_arg) != 0)
        error_handler("pthread_create");

    if (pthread_create(&thread_id_vector[2], NULL, producer_thread, (void *) &weather_arg) != 0)
        error_handler("pthread_create");

    if (pthread_create(&logger_id, NULL, logger_thread, (void *) &logger_arg) != 0)
        error_handler("pthread_create");

    for (int i = 0; i < THREAD_NO; i++) {
        if (pthread_join(thread_id_vector[i], NULL) != 0)
            error_handler("pthread_join");
    }

    if (pthread_join(logger_id, NULL) != 0)
        error_handler("pthread_join");

    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&cond1);
    pthread_cond_destroy(&cond2);

    return EXIT_SUCCESS;
}

