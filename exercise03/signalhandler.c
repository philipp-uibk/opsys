#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>



void exit_proc(int signum){
    exit(0);
}

int main(void){
    pid_t current_pid = fork();
    
    switch(current_pid){
        case -1:
            perror("fork");
            return EXIT_FAILURE;
        case 0:
            printf("My pid is %u\n",getpid());
            exit(0);
        default:;
            struct sigaction action, old_action;
            action.sa_handler = exit_proc;
            sigemptyset(&action.sa_mask);
            //ensures child doesnt turn into zombie, nocldstop flag only sends signal when child war terminated not stopped
            action.sa_flags = SA_NOCLDSTOP | SA_NOCLDWAIT;
            sigaction(SIGCHLD,&action,&old_action);
            pause();
    }
    perror("error in main");
    return EXIT_FAILURE;
}

