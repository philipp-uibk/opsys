#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

#define PROC_NO 12

int main(void){
    for (int i = 0; i < PROC_NO; i++) {
        switch (fork()) {
            case 0:
                printf("%u\n", getpid());
                exit(0);
            case -1:
                perror("fork");
                return EXIT_FAILURE;
        }
    }

    printf("12 child processes have been created\n");

    //wait for all processes
    for (int i = 0; i < PROC_NO; i++) {
        wait(0);
    }

    printf("waited for all processes");


    return EXIT_SUCCESS;
}