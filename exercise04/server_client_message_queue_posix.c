#define _GNU_SOURCE

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <time.h>
#include <string.h>
#include <sys/select.h>
#include <limits.h>
#include <sys/fcntl.h>
#include <mqueue.h>
#include <signal.h>

#define MSQ_NAME "/MSQIW\0"
#define BUF_SIZE 1024

void err_sys(const char *x) { //function to close process
    perror(x);
    fcloseall();
    exit(EXIT_FAILURE);
}

mqd_t receiver_msq;
mqd_t sender_msq;
mqd_t main_msq;

//signal handler, if wheter 1 child terminates, or a parent is aborted, sigint is sent to all other processes, which then terminate itself
void signal_handler(int signal) {
    switch (signal) {
        case SIGTERM:
            //case SIGCHLD:
        case SIGINT:
            //On process stop unlink all fifos, kill all children and parent
            mq_close(receiver_msq);
            mq_close(sender_msq);
            mq_close(main_msq);
            mq_unlink(MSQ_NAME);

            kill(0, SIGINT);
            err_sys("one child or parent terminated unexpectedly");
            exit(EXIT_FAILURE);
    }
}


void create_server_process(char *message) {
    switch (fork()) {
        case -1:
            err_sys("fork");
            break;
        case 0:;
            //open receiving message queue
            if ((sender_msq = mq_open(MSQ_NAME, O_WRONLY)) < 0)
                err_sys("mq_open sender");

            while (1) {
                sleep(rand() % 6 + 2);
                mq_send(sender_msq, message, strlen(message) + 1, 1);
            }
    }
}

int main(int argc, char **argv) {
    struct mq_attr attr;
    attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = 20;

    //creating message queue
    if ((main_msq = mq_open(MSQ_NAME, O_CREAT | O_RDWR, 0644, &attr)) < 0)
        err_sys("mq_open main");

    //signal handling
    struct sigaction action;
    action.sa_handler = signal_handler;
    action.sa_flags = SA_NOCLDWAIT;
    sigemptyset(&action.sa_mask);

    sigaction(SIGINT, &action, NULL);
    sigaction(SIGCHLD, &action, NULL);
    sigaction(SIGTERM, &action, NULL);

    //random number generator init
    srand(time(NULL));

    //create three processes
    create_server_process("web");
    create_server_process("mid");
    create_server_process("dat");

    //last process
    switch (fork()) {
        case -1:
            err_sys("fork");
        case 0:;
            //receiver process
            if ((receiver_msq = mq_open(MSQ_NAME, O_RDONLY)) < 0)
                err_sys("mq_open receiver");

            char received_message[BUF_SIZE];

            while (1) {
                if (mq_receive(receiver_msq, received_message, BUF_SIZE, NULL) < 0)
                    err_sys("mq_receive");
                printf("[%s] received from server\n", received_message);
            }

    }
    pause();
    return EXIT_SUCCESS;
}