#define _GNU_SOURCE
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include <sys/ipc.h>
#include <sys/msg.h>


const int MESSAGE_SIZE = 25;
const int KEY = 1025;

typedef struct mssgbuf {
	long mtype;
	char mtext[25];
} message_buf;

void error_handler(const char *x) {
    perror(x);
    exit(EXIT_FAILURE);
}

void create_server_process(char *fifoname){
    //creating child, parent returns immediately
    switch(fork()){
        case -1:
            error_handler("fork");
            break;
        case 0:;
            message_buf send_buffer;
            size_t buf_length;
            int msqid; 
            if((msqid = msgget(KEY,IPC_CREAT | 0666))<0)
                error_handler("msgget");
				
			send_buffer.mtype = 1;
			strcpy(send_buffer.mtext,fifoname);
			buf_length = MESSAGE_SIZE;
			
            while(1){
                sleep(rand()%6+2);
                msgsnd(msqid,&send_buffer,buf_length,IPC_NOWAIT);
            }
            break;
    }
}


int main(int argc, char** argv){
    srand(time(NULL));
    
    char message_webserver[] = "web";
    char message_middlewareserver[] = "middle-ware";
    char message_database[] = "database";


    if (strlen(message_webserver) > MESSAGE_SIZE) error_handler("MESSAGE_SIZE1");
    if (strlen(message_middlewareserver) > MESSAGE_SIZE) error_handler("MESSAGE_SIZE2");
    if (strlen(message_database) > MESSAGE_SIZE) error_handler("MESSAGE_SIZE3");

	
    create_server_process(message_webserver);
    create_server_process(message_middlewareserver);
    create_server_process(message_database);

    //last process
    switch(fork()){
        case -1:
            error_handler("fork");
        case 0:;

			int msqid;
			message_buf receiving_buffer;
			
			msqid = msgget(KEY,0666);
            while(1){
				msgrcv(msqid,&receiving_buffer,MESSAGE_SIZE,1,0);
				
				fprintf(stdout,"[%s] message received\n",receiving_buffer.mtext);
            }
       

    }
    wait(0);

    return EXIT_SUCCESS;

}
