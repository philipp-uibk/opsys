#include "common_posix.h"

void err_sys(const char *x){ //function to close process
    perror(x);
    fcloseall();
    exit(EXIT_FAILURE);
}

int main(void){
    int shm_fd;
    int* counter; 
    int fifo_fd;

    //make fifo
    if ((mkfifo(FIFO, 0666)) < 0) {
        if (errno != EEXIST) {
            err_sys("mkfifo");
        }
    }

    //create shared memory, getting file descriptor of memory
    if( (shm_fd = shm_open(SHM_ID,O_RDWR | O_CREAT ,S_IRWXU | S_IRWXO | S_IRWXG) ) < 0 )
        err_sys("shm_open");

    //extend shared memory to correct size
    if( (ftruncate(shm_fd,SHM_SIZE)) < 0 )
        err_sys("ftruncate");

    //map shared memory to process adress space
    if( (counter = mmap(NULL,SHM_SIZE,PROT_READ | PROT_WRITE,MAP_SHARED,shm_fd,0)) == (void *) -1 )
        err_sys("mmap");

    //setting value to 0
    *counter = 0;

    //open fifo for reading
    if( (fifo_fd = open(FIFO,O_RDONLY)) == -1 )
        err_sys("open read");

    //we receive string from fifo
    char buffer[BUF_SIZE];

    if( (read(fifo_fd,buffer,BUF_SIZE)) > 0 )
        fprintf(stdout,"receiver received: %ld\n",strtol(buffer,NULL,10));

    if( close(fifo_fd) < 0 )
        err_sys("close");

    //delete FIFO
    unlink(FIFO);

    //unmap (detach) shared memory from address space
    if( (munmap(counter,SHM_SIZE)) < 0 )
        err_sys("munmap");
    return EXIT_SUCCESS;

}