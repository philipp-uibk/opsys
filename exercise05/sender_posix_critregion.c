#include "common_posix.h"

void err_sys(const char *x){ //function to close process
    fcloseall();
    perror(x);
    exit(EXIT_FAILURE);
}

int main(void){
    int children = 1000;
    int additions = 10000;

    int shm_fd;
    int *counter;

    //create shared memory, getting file descriptor of memory
    if( (shm_fd = shm_open(SHM_ID,O_RDWR | O_CREAT ,S_IRWXU | S_IRWXO | S_IRWXG) ) < 0 )
        err_sys("shm_open");


    //map shared memory to process adress space
    if( (counter = mmap(NULL,SHM_SIZE,PROT_READ | PROT_WRITE,MAP_SHARED,shm_fd,0)) == (void *) -1 )
        err_sys("mmap");


    sem_t* sem;
    unsigned int value = 1;

    //open and create semaphore with name SEM_ID, with value value
    sem = sem_open(SEM_ID,O_CREAT,0666,value);

  
    for(int i = 0;i<children;i++){
        switch(fork()){
            case -1:
                sem_unlink(SEM_ID);
                sem_close(sem);
                err_sys("fork");
                break;
            case 0:
                for(int j=0;j<additions;j++){
                    
                    //decrement semaphore
                    sem_wait(sem);

                    //critical region
                    (*counter)++;

                    //increment semaphore
                    sem_post(sem);

                }
                exit(EXIT_SUCCESS);
        }
    }
    
    //wait for all children
    while( wait(NULL) > 0);
    
    //delete semaphore
    if( sem_unlink(SEM_ID) < 0 )
        err_sys("sem_unlink");

    if( sem_close(sem) < 0 )
        err_sys("sem_close");

    //print result when all children have finished
    fprintf(stdout,"Result from sender: %d\n",*counter);

    //write result to pipe
    FILE* fifo_fp;
    if( (fifo_fp = fopen(FIFO,"w")) == NULL )
        err_sys("fopen w");

    fprintf(fifo_fp,"%d",*counter);

    //close fifo
    if( fclose(fifo_fp) < 0 )
        err_sys("fclose");
    
    //unmap (detach) shared memory from address space
    if( (munmap(counter,SHM_SIZE)) < 0 )
        err_sys("munmap");
    return EXIT_SUCCESS;


    //delete shared memory id 
    if( (shm_unlink(SHM_ID)) < 0)
        err_sys("shm_unlink");
    

    return EXIT_SUCCESS;

}