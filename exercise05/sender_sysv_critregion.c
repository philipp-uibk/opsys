#include "common_sysv.h"


void err_sys(const char *x){ //function to close process
    fcloseall();
    perror(x);
    exit(EXIT_FAILURE);
}

int main(void){
    int children = 1000;
    int additions = 10000;

    int shmid;
    int *counter;

    //get shared memory, if it doesnt exist, create it
    if( (shmid = shmget(SHM_KEY,SHM_SIZE,IPC_CREAT|0666)) < 0 )
        err_sys("shmget");

    //attach shared memory
    if( (counter = shmat(shmid,NULL,0)) < 0)
        err_sys("shmat");

    //semget(key_t key, int nsems, int semflag)
    int semid;
    if( (semid = semget(SEM_KEY,1,IPC_CREAT | 0666)) < 0 )
        err_sys("semget");


    //decrement struct semaphore
    struct sembuf semwait[1];
    semwait[0].sem_num = 0;
    semwait[0].sem_op = -1;
    semwait[0].sem_flg = 0;

    //incrementstruct semaphore
    struct sembuf semsignal[1];
    semsignal[0].sem_num = 0;
    semsignal[0].sem_op = 1;
    semsignal[0].sem_flg = 0;

  
    //increment semaphore
    if( semop(semid,semsignal,1) < 0 )
        err_sys("semop init");
  
    for(int i = 0;i<children;i++){
        switch(fork()){
            case -1:
                semctl(semid,0,IPC_RMID);
                err_sys("fork");
                break;
            case 0:
                for(int j=0;j<additions;j++){
                    
                    //decrement semaphore, if decrementation is not possible stay in while
                    while( (semop(semid,semwait,1)) == -1 );

                    //critical region
                    (*counter)++;

                    //increment semaphore
                    while( (semop(semid,semsignal,1)) == -1) ;

                }
                exit(EXIT_SUCCESS);
        }
    }
    
    //wait for all children
    while( wait(NULL) > 0);

    //delete semaphore
    if( semctl(semid,0,IPC_RMID) < 0 )
        err_sys("semctl remove"); 
    
    //print result when all children have finished
    fprintf(stdout,"Result from sender: %d\n",*counter);

    //write result to pipe
    FILE* fifo_fp;
    if( (fifo_fp = fopen(FIFO,"w")) == NULL )
        err_sys("fopen w");

    fprintf(fifo_fp,"%d",*counter);

    //close fifo
    if( fclose(fifo_fp) < 0 )
        err_sys("fclose");
    
    //detach shared memory
    if( shmdt(counter) < 0 )
        err_sys("shmdt");

    //delete shared memory id 
    if( shmctl(shmid,IPC_RMID,0) < 0 )
        err_sys("shmctl");

    
    return EXIT_SUCCESS;

}