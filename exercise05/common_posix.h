#define _GNU_SOURCE
#define _POSIX_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <semaphore.h>

#define BUF_SIZE 32
#define SHM_SIZE 32
#define FIFO "RESULT_FIFO"
#define SEM_ID "sem"
#define SHM_ID "/SHM_T"
