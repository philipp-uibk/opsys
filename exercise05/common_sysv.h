#define _GNU_SOURCE
#define _POSIX_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/sem.h>

#define BUF_SIZE 32
#define SHM_SIZE 32
#define FIFO "RESULT_FIFO"
#define SHM_KEY 222
#define SEM_KEY 55L

