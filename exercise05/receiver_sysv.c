#include "common_sysv.h"

/**
 * output analysis:
 * 
 * the output is different, and always less than 10.000.000
 * this is because *counter++ isnt atomic
 * 
 * the output sent to the fifo and the output generated output is the same
 */

void err_sys(const char *x){ //function to close process
    perror(x);
    fcloseall();
    exit(EXIT_FAILURE);
}

int main(void){
    int shmid;
    int* counter; 
    int fifo_fd;

    //make fifo
    if( (mkfifo(FIFO,0666)) < 0 )
        err_sys("mkfifo");

    //create shared memory
    if( (shmid = shmget(SHM_KEY,SHM_SIZE,IPC_CREAT|0666)) < 0 )
        err_sys("shmget");

    //attach shared memory
    if( (counter = shmat(shmid,NULL,0)) < 0 )
        err_sys("shmat");

    //setting value to 0
    *counter = 0;
    
    
    if( (fifo_fd = open(FIFO,O_RDONLY)) == -1 )
        err_sys("open read");

    char buffer[BUF_SIZE];

    if( (read(fifo_fd,buffer,BUF_SIZE)) > 0 )
        fprintf(stdout,"receiver received: %ld\n",strtol(buffer,NULL,10));

    if( close(fifo_fd) < 0 )
        err_sys("close");
    
    unlink(FIFO);

    if( shmdt(counter) < 0 )
        err_sys("shmdt");

    return EXIT_SUCCESS;

}
