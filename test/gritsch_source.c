//gcc -Wall -Werror -std=gnu11 -pthread gritsch_source.c -o bounded_buffer && ./bounded_buffer 5 25
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


typedef struct argument {
    int internal_thread_id;
    int return_value;
} thread_argument;

typedef struct bounded_buffer{
    int last_idx;
    int buff_size;
    int *buff;
} bounded_buf_t;

//following 2 functions are from my solution exercise 7, task1 common.h
void error_handler(char *msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

void error_handler_thread(char *msg, int allocated, void *adress_vector[]) {
    perror(msg);
    pthread_exit(NULL);
}

size_t fib(int n){
    if(n == 0)
        return 0;

    if(n == 1)
        return 1;
    return fib(n-1)+fib(n-2);
}

///////////////////////////////////////////////////////////////////////////////

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t element_available = PTHREAD_COND_INITIALIZER;
pthread_cond_t element_consumed = PTHREAD_COND_INITIALIZER;

bounded_buf_t bounded_buffer_memory;

void *consumer(void *arg){
    thread_argument* local_argument = (thread_argument*) arg;
    unsigned consumer_sum = 0, consumed = 0;
    for(int i = 0; i< 20; i++){

        pthread_mutex_lock(&mutex);

        while(bounded_buffer_memory.buff_size == 0){
            pthread_cond_wait(&element_available,&mutex);
        }

        //we are able to access buffer
        consumed=bounded_buffer_memory.buff[bounded_buffer_memory.last_idx];

        consumer_sum+=fib(consumed);

        //decrement size, decrement last index
        bounded_buffer_memory.last_idx--;
        bounded_buffer_memory.buff_size--;
        //this is the case everytime, we consumed
        pthread_cond_signal(&element_consumed);

        pthread_mutex_unlock(&mutex);
    }

    fprintf(stdout,"consumer %d: consumer_sum %d\n", local_argument->internal_thread_id, consumer_sum );
    local_argument->return_value = consumer_sum;

    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    if (argc != 3)
        error_handler("argc");

    int consumer_no, buf_size;

    if( (consumer_no = (int) strtol(argv[1],NULL, 10)) < 0 )
        error_handler("negative argument");

    if ( ( buf_size = (int) strtol(argv[2],NULL,10)) < 0)
        error_handler("negative argument");

    int* buffer;
    if( (buffer = malloc(buf_size*sizeof(int))) == NULL ) {
        error_handler("malloc");
    }

    srand(703);

    bounded_buffer_memory.buff_size = 0;
    bounded_buffer_memory.buff = buffer;
    bounded_buffer_memory.last_idx = 0;

    pthread_t thread_id_vector[consumer_no];
    thread_argument argument_vector[consumer_no];

    for (int i = 0; i < consumer_no; i++) {
        argument_vector[i].internal_thread_id = i+1;
        argument_vector[i].return_value = 0;
        if (pthread_create(&thread_id_vector[i], NULL, consumer, (void *) &argument_vector[i]) != 0) {
            error_handler("pthread_create");
        }
    }

    for(int i = 0; i < 20*consumer_no; i++){
        pthread_mutex_lock(&mutex);

        while(bounded_buffer_memory.buff_size >= buf_size-1){
            pthread_cond_wait(&element_consumed,&mutex);
        }

        //didnt want to waste time, so I simply copied from https://www.geeksforgeeks.org/generating-random-number-range-c/
        int num = (rand() % (15 - 5 + 1)) + 5;

        //last idx points to last full element, so we have to increase it first
        bounded_buffer_memory.last_idx++;
        bounded_buffer_memory.buff[bounded_buffer_memory.last_idx] = num;
        bounded_buffer_memory.buff_size++;

        pthread_cond_signal(&element_available);
        pthread_mutex_unlock(&mutex);
    }


    unsigned producer_sum = 0;
    for (int i = 0; i < consumer_no; i++) {
        if (pthread_join(thread_id_vector[i],NULL) != 0) {
            error_handler("pthread_join");
        } else {
            fprintf(stdout,"producer: the consumer %d computed the sum %d\n", argument_vector[i].internal_thread_id, argument_vector[i].return_value);
            producer_sum+=argument_vector[i].return_value;
        }
    }

    fprintf(stdout,"producer: total_sum = %d\n", producer_sum);

    free(buffer);
    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&element_consumed);
    pthread_cond_destroy(&element_available);
    return EXIT_SUCCESS;
}
