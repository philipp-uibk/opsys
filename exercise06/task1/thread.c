#define _POSIX_SOURCE
#define _GNU_SOURCE

#include <pthread.h>
#include <sys/syscall.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

#define THREAD_NO 10
#define FILE_MODE_OPEN "w+"

void error_handler(char* msg){
    perror(msg);
    exit(EXIT_FAILURE);
}

void error_handler_thread(char *msg){
    perror(msg);
    pthread_exit(NULL);
}

void cleanup_handler(void *fp){

    FILE* filepointer = (FILE*) fp;

    if( fclose(filepointer) != 0 )
        error_handler_thread("fclose");


}

void *start_routine(void *arg){

    FILE* fp;
    char filename[] = "threadN.txt";
    int* number = (int*) arg;
    filename[6] = *number + '0';

    //wait for random time 0 - 3 s
    sleep(rand()%4);

    //thread not cancelable while creating file (not atomic)
    if( pthread_setcancelstate(PTHREAD_CANCEL_DISABLE,NULL) != 0 )
        error_handler_thread("pthread_setcancelstate");

    //create file
    if( (fp = fopen(filename,FILE_MODE_OPEN)) == NULL )
        error_handler_thread("fopen");

    //control message
    fprintf(stdout,"thread %d has created the file\n",*number);

    //add the cleanup handler, as now the file has been opened by the thread
    pthread_cleanup_push(cleanup_handler,fp);

    //handler defined -> set cancelable true
    if( pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,NULL) != 0 )
        error_handler_thread("pthread_setcancelstate");

    //print thread ID to file
    fprintf(fp,"%ld\n",pthread_self());

    //control message
    fprintf(stdout,"thread %d has written to file\n",*number);

    //needed for cleanup-push, altouhg pthread exit does it automatically
    pthread_cleanup_pop(1);

    //exit thread
    pthread_exit(NULL);
}


int main(void){

    //setting random seeder
    srand(time(NULL));

    //vector for storing the thread id and its number
    pthread_t thread_id_vector[THREAD_NO];
    int creation_number_vector[THREAD_NO];

    //create THREAD_NO threads, safe creation order in creation number vector
    for(int i = 0; i < THREAD_NO; i++){
        creation_number_vector[i] = i;

        //create thread
        if(pthread_create(&thread_id_vector[i],NULL,start_routine,(void *) &creation_number_vector[i]) != 0)
            error_handler("pthread_create");

    }

    //decide on each thread if we want to cancel him
    for(int i = 0; i<THREAD_NO; i++){
        if(rand()%2){
            fprintf(stdout,"Cancel-Request sent to thread: %d\n",i);
            if( pthread_cancel(thread_id_vector[i]) != 0 )
                error_handler("pthread_cancel");
        }
    }

    //wait for all threads, also the cancelled ones (will return immediately)
    for(int i = 0; i<THREAD_NO; i++){
        pthread_join(thread_id_vector[i], NULL);
    }

    return EXIT_SUCCESS;
}

