#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>

#define EAT_DURATION 500
#define NUM_PHILOSOPHER 5
#define EAT_ITERATIONS 1000
#define RIGHT_CHOPSTICK(n) (n)
#define LEFT_CHOPSTICK(n) (((n) + 1) % NUM_PHILOSOPHER)

//
#include <semaphore.h>
#include <fcntl.h>
#define MODE 0666
#define SEM_ID "sem"
//

pthread_t philosopher[NUM_PHILOSOPHER];
pthread_mutex_t chopstick[NUM_PHILOSOPHER];

//global semaphore
sem_t* semaphore;

/*
 * Dining philosophers-problem:
 *
 * To eat you need 2 chopsticks.
 * In this case we create 5 philosophers with 5 chopsticks.
 * So every philospher tries to pick up 2 sticks (a left and a right one)
 * represented by 5 mutexes.
 *
 * In the case that each philosopher picks up one stick, every mutex is locked.
 * And no one can eat, because there are no mutexes left.
 * And as no one is willing to give up a stick, no one gets eat. -> Deadlock
 *
 * The deadlock won't occur when at least one philosopher is able to pick up two sticks, because after eating
 * he unlocks both sticks, making it possible for at least another philosopher to eat.
 *
 * The deadlock doesn't always occur. It depends on the scheduling of the OS.
 * If every thread is scheduled, if it picks up one stick, it happens. If at least one thread gets to lock both mutexes,
 * the problem doesn't occur.
 *
 * I've changed the program in that way, that only four philosophers are allowed to pick up a chopstick.
 * So there is at least one philosopher which can pick 2, avoiding that no one can eat, avoiding the deadlock.
 *
 */

void *dine(void *id) {
    int n = (int)(intptr_t) id;
    for (int i = 0; i < EAT_ITERATIONS; ++i) {
        //added another critical region, to allow 4 threads at maximum to pick a chopstick
        sem_wait(semaphore);
        pthread_mutex_lock(&chopstick[RIGHT_CHOPSTICK(n)]);
        pthread_mutex_lock(&chopstick[LEFT_CHOPSTICK(n)]);
        sem_post(semaphore);

        usleep(EAT_DURATION);
        pthread_mutex_unlock(&chopstick[LEFT_CHOPSTICK(n)]);
        pthread_mutex_unlock(&chopstick[RIGHT_CHOPSTICK(n)]);
    }
    printf("Philosopher %d is done eating!\n", n);

    return (NULL);
}

int main() {


    //4 philosophers can pick
    unsigned int sem_value = 4;

    //open semaphore
    semaphore = sem_open(SEM_ID,O_CREAT,MODE,sem_value);


    for (int i = 0; i < NUM_PHILOSOPHER; ++i) {
        pthread_mutex_init(&chopstick[i], NULL);
    }

    for (int i = 0; i < NUM_PHILOSOPHER; ++i) {
        pthread_create(&philosopher[i], NULL, dine, (void *) (intptr_t) i);
    }

    for (int i = 0; i < NUM_PHILOSOPHER; ++i) {
        pthread_join(philosopher[i], NULL);
    }

    for (int i = 0; i < NUM_PHILOSOPHER; ++i) {
        pthread_mutex_destroy(&chopstick[i]);
    }

    //unlink and destroy semaphore
    if( sem_unlink(SEM_ID) < 0){
        perror("sem_unlink");
        exit(EXIT_FAILURE);
    }

    if( sem_close(semaphore) < 0){
        perror("sem_close");
        exit(EXIT_FAILURE);
    }

    return EXIT_SUCCESS;
}

