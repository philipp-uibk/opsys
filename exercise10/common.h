#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>
#include <time.h>

typedef struct thread_arg {
    int ALLOC_SZ;
    int ALLOC_NO;
} threadarg_t;

void error_handler(char *msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

void error_handler_thread(char *msg, int allocated, void *adress_vector[]) {
    for (int i = 0; i < allocated; i++) {
        free(adress_vector[i]);
    }
    perror(msg);
    pthread_exit(NULL);
}

