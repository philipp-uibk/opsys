#include "common.h"
#include "own_bestfit_local.h"


void *allocator(void *arg) {
    if (init_own_memory() == EXIT_FAILURE)
        error_handler_thread("init memory", 0, NULL);

    threadarg_t *threadarg = (threadarg_t *) arg;
    void *adress_vector[threadarg->ALLOC_NO];
    unsigned thread_seed = (unsigned) pthread_self();

    //allocate memory alloc times often
    for (int i = 0; i < threadarg->ALLOC_NO; i++) {
        //alloc allocate size
        unsigned real_seed = thread_seed + i;
        unsigned effective_size = (rand_r(&real_seed) % (7 * threadarg->ALLOC_SZ + 1)) + threadarg->ALLOC_SZ;
        if ((adress_vector[i] = own_malloc(effective_size)) == NULL)
            error_handler_thread("malloc", i, adress_vector);
    }

    //free half of the space
    for (int i = 0; i < threadarg->ALLOC_NO; i++) {
        unsigned real_seed = thread_seed + i;
        if (rand_r(&real_seed) % 2) {
            own_free(adress_vector[i]);
            adress_vector[i] = NULL;
        }
    }

    //allocate another N/2 memory segments
    void *other_adress_vector[threadarg->ALLOC_NO / 2];
    for (int i = 0; i < threadarg->ALLOC_NO / 2; i++) {
        //alloc allocate size
        unsigned real_seed = thread_seed + i;
        unsigned effective_size = (rand_r(&real_seed) % (7 * threadarg->ALLOC_SZ + 1)) + threadarg->ALLOC_SZ;
        if ((other_adress_vector[i] = own_malloc(effective_size)) == NULL)
            error_handler_thread("malloc", i, adress_vector);
    }

    //free the rest of original values
    for (int i = 0; i < threadarg->ALLOC_NO; i++) {
        if (adress_vector[i] != NULL) {
            own_free(adress_vector[i]);
        }
    }

    //free the rest of the second values
    for (int i = 0; i < threadarg->ALLOC_NO / 2; i++) {
        own_free(other_adress_vector[i]);
    }

    free_own_memory();
    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    if (argc != 4)
        error_handler("argc");

    int THREAD_NO = (int) strtol(argv[1], NULL, 10);

    threadarg_t threadarg;
    threadarg.ALLOC_NO = (int) strtol(argv[2], NULL, 10);
    threadarg.ALLOC_SZ = (int) strtol(argv[3], NULL, 10);

    pthread_t thread_vector[THREAD_NO];

    //create THREAD_NO threads
    for (int i = 0; i < THREAD_NO; i++) {
        if (pthread_create(&thread_vector[i], NULL, allocator, (void *) &threadarg) != 0)
            error_handler("thread_create");
    }

    //wait for threads
    for (int i = 0; i < THREAD_NO; i++) {
        if (pthread_join(thread_vector[i], NULL) != 0)
            perror("pthread_join");
    }


    return EXIT_SUCCESS;
}