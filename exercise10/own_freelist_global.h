#ifndef _STDIO_H
#include <stdio.h>
#endif

#ifndef _STDLIB_H
#include <stdlib.h>
#endif

#ifndef _STDBOOL_H

#include <stdbool.h>

#endif

#ifndef _PTHREAD_H
#include <pthread.h>
#endif

#ifndef _MATH_H

#include <math.h>

#endif

typedef struct node {
    struct node *list;
    struct node *next;
    bool used;
} node_t;

#define POWERS 21


pthread_mutex_t mutex_array[POWERS];
node_t *list_array[POWERS];

//returns pointer to the sentinel node of a new empty list, on error NULL
node_t *init_list() {
    node_t *start_node;
    if ((start_node = malloc(sizeof(node_t))) == NULL) {
        return NULL;
    }

    start_node->next = NULL;
    start_node->used = true;
    start_node->list = start_node;

    return start_node;
}

//extended add: creates or uses a node already in the list, with size size
//returns NULL on failure and pointer to node on success
node_t *add_to_list(node_t *sentinel_node, size_t size) {
    node_t *current_node = sentinel_node;
    node_t *previous_node = NULL;

    //traverse list, stops at first not used or at the last element
    while (current_node != NULL && current_node->used) {
        previous_node = current_node;
        current_node = current_node->next;
    }

    if (current_node == NULL) {
        if ((current_node = malloc(sizeof(node_t) + size)) == NULL) {
            return NULL;
        }

        current_node->next = NULL;
        current_node->used = true;
        current_node->list = sentinel_node;
        previous_node->next = current_node;
    }

    return current_node;

}

//sets the node element to used false
//returns 0 on failure
int set_used_false(node_t *sentinel_node, node_t *node_to_delete) {
    node_t *current_node = sentinel_node;

    //traverses list, stops at element, which has to be set 0
    while (current_node != NULL && current_node != node_to_delete) {
        current_node = current_node->next;
    }

    if (current_node == NULL)
        return 0;
    current_node->used = false;
    return 1;
}


void free_list(node_t *sentinel_node) {
    node_t *current_node = sentinel_node;
    node_t *next = NULL;

    while (current_node != NULL) {
        next = current_node->next;
        free(current_node);

        current_node = next;
    }
}

int init_own_memory() {
    for (int i = 0; i < POWERS; i++) {
        if ((list_array[i] = init_list()) == NULL) {
            perror("init_list");
            return EXIT_FAILURE;
        }

        pthread_mutex_init(&(mutex_array[i]), NULL);

    }

    return EXIT_SUCCESS;
}

void free_own_memory() {
    for (int i = 0; i < POWERS; i++) {
        free_list(list_array[i]);
        pthread_mutex_destroy(&(mutex_array[i]));
    }

}

void *own_malloc(size_t size) {
    int position;
    unsigned current_power = 0;
    node_t *new_node = NULL;

    //traverse till the right power is found
    for (position = POWERS - 1; position >= 0; position--) {
        current_power = pow(2, position);
        if (size <= current_power) {
            break;
        }
    }


    pthread_mutex_lock(&mutex_array[position]);
    if ((new_node = add_to_list(list_array[position], current_power)) == NULL) {
        return NULL;
    }
    pthread_mutex_unlock(&mutex_array[position]);
    return (void *) new_node + sizeof(node_t);
}

void own_free(void *pointer) {
    node_t *node_to_free = pointer - sizeof(node_t);

    int position = 0;
    for (position = 0; position <= POWERS; position++) {
        if (list_array[position] == node_to_free->list) {
            break;
        }
    }

    if (position == POWERS) {
        printf("You omitted a wrong pointer");
        return;
    }

    pthread_mutex_lock(&mutex_array[position]);
    if (set_used_false(node_to_free->list, node_to_free) == 0) {
        perror("you omitted a wrong pointer\n");
    }
    pthread_mutex_unlock(&mutex_array[position]);
}