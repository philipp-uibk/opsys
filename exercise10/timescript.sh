#!/usr/bin/env bash

PROGRAM="./membench_bestfit_local"
COUNTS="1000"
#consider that 2000 is already too much for bestfit_local as 65536 * 2000 * 8 is smaller than 1 GB but the managment space included its too large


for THREAD in 1 2 4 8
do
    for SIZE in 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536
    do
        echo "T: "$THREAD "N: "$COUNTS "S: "$SIZE
        /usr/bin/time -f "%E" $PROGRAM $THREAD $COUNTS $SIZE
    done
done

