cmake_minimum_required(VERSION 3.14)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=gnu11 -Wall -Werror ")

add_executable(ex08_scheduling
        main.c
        scheduling_sim.c
        scheduling_sim.h
        scheduling_utility.c
        scheduling_utility.h
        )

target_link_libraries(ex08_scheduling m)