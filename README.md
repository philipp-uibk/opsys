# Operating Systems

The repository uses following commit flags:

`FILE` or `CMAKE` for file operations, moving, copying, renaming, nothing with code.

`ADD` for adding features

`ALTER` for altered features, improvements,

`MOD` in comparison to ALTER flag, extends existing functions/methods

`DEL` should be self-explaining

<br><br>
This is the repository for the lecture and the proseminar of operating
systems.

For the single tasks, visit each separate folder

| Folder                   | Description                                                                                             |
|:-------------------------|:--------------------------------------------------------------------------------------------------------|
| [exercise01](exercise01) | Simple bash scripts                                                                                     |
| [exercise02](exercise02) | Scheduling on paper                                                                                     |
| [exercise03](exercise03) | Signals, Signalhandler, Blocking different signals                                                      |
| [exercise04](exercise04) | Processes: Fifos, Message Queues (Sys-V and POSIX), Pipes (pipe)                                        |
| [exercise05](exercise05) | Processes: Shared Memory, Fifo for IPC, Consumer and Producer Processes                                 |
| [exercise06](exercise06) | Threads:  Simple examples, Cleanup-Handler, Consumer-Producer with Queue and Mutex, dining philosophers |
| [exercise07](exercise07) | Threads: Consumer-Producer improved with Mutex, Condition variables and spinlocks, as well as atomics   |
| [exercise08](exercise08) | Scheduling utility, implemented some scheduling algorithms                                              |
| [exercise09](exercise09) | Memory Allocators: Best-Fit Allocator implementation with linked structure                              |
| [exercise10](exercise10) | Memory Allocators: Free List Allocator, with linked list implementation                                 |
